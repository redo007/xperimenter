# X Perimenter

## Concepto:
Juego 2D en el que se debe ayudar a disintos sujetos experimentales a escapar de habitaciones mediante la activación de palancas, botones u otros artefactos.
La dificultad radica en que cada sujeto tiene un tiempo de vida muy breve, pero mientras su cuerpo esté en el escenario, puede ser de utilidad. 

## Género:
Plataformas / Puzzle

## Plataforma:
PC

## Audiencia:
Mujeres y hombres de 20 a 30 años que se entretengan resolviendo acertijos con un toque de humor.

## Elementos del juego:
* __Sujetos Experimentales__: son controlados por el jugador y tienen un tiempo de vida muy corto (no más de 30 seg.). Pueden moverse, saltar e interactuar con los objetos.
* __Puerta__: el objetivo de cada nivel, es abrir la puerta y salir de la sala.
* __Objetos__: sirven para activar la puera, pueden funcionar individualmente o en conjunto. Son:
  * *Palancas*: Se activan o desactivan.
  * *Botones*: Deben mantenerse presionados
  * *Cadáver de un sujeto*: Puede arrastrarse por el nivel. Después de un tiempo desaparece.
  * *Licuadoras*: se activan cuando un sujeto entra en ellas. Generan fluído amniótico.

## Desarrollo del juego:
El jugador comienza en la habitación con la puerta cerrada. Su objetivo: abrirla y escaparse.
Los sujetos aparecen de a uno y en todo momento hay sólo uno vivo en la sala. Cuando este muere, aparece uno nuevo.
Una vez activados los mecanismos, la puerta se abre y se debe llevar a un sujeto hasta ella para terminar el nivel.

Para hacer más interesantes los niveles, puede haber un límite de tiempo y/o de sujetos a utilizar.

![DemoGif](https://media.giphy.com/media/5nbDCZq6UI5yzoO5Cr/giphy.gif)

*Prototipo del juego hecho con FlashPunk*

## Estética:
La estética general del juego toma elementos de los juegos Portal y Portal 2 y de la saga de películas Saw (pero sin gore).
Si bien, la mayoría de los sujetos experimentales terminan muriendo, la idea es que sea algo más caricaturesco y divertido.
Ejemplo: en lugar de sangre, hay confites o algún fluído de múltiples colores.






